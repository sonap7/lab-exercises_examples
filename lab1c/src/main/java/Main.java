import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(2, 5, 5, 5, 5, 6, 6, 8, 9, 9, 9);

        final int target = 5;

        long res = list.stream().filter(i->i==target).count();
        System.out.println("Element "+target+" occurs "+res+" times.");

    }
}
