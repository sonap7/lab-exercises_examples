public class BinarySearchTree {
    // Root of BST
    private Node root;

    // Constructor
    BinarySearchTree() {

        root = null;
    }

    // This method calls insertRec()
    public void insert(int key) {

        root = insertRec(root, key);
    }

    /* A function to insert a new key in BST */
    public Node insertRec(Node root, int key) {

        /* If the tree is empty, create amd return a new node */
        if (root == null) {
            root = new Node(key);
            return root;
        }

        if (key < root.key) {
            root.left = insertRec(root.left, key);
        }
        if (key > root.key) {
            root.right = insertRec(root.right, key);
        }

        /* return the node pointer */
        return root;
    }

    // This method calls InorderRec()
    public void inorder() {
        inorderRec(root);
    }

    // A function to do in order
    public void inorderRec(Node root) {
        if (root != null) {
            inorderRec(root.left);
            System.out.println(root.key);
            inorderRec(root.right);
        }
    }

    public int minNode() {
        Node tempMin = root;

        while (tempMin.left != null) {
            tempMin = tempMin.left;
        }
        System.out.println(tempMin.key);
        return (tempMin.key);
    }

    public int maxNode() {
        Node tempMax = root;

        while (tempMax.right != null) {
            tempMax = tempMax.right;
        }
        System.out.println(tempMax.key);
        return (tempMax.key);
    }
}