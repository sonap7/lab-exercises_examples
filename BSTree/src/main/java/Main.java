import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        BinarySearchTree tree = new BinarySearchTree();
        int choise;

        tree.insert(50);
        tree.insert(30);
        tree.insert(20);
        tree.insert(40);
        tree.insert(70);
        tree.insert(60);
        tree.insert(80);

        do{
            System.out.println("1.Add a node\n\r2.Show min node\n\r3.Show max node\n\r6.Print all nodes");
            Scanner in = new Scanner(System.in);
            choise = in.nextInt();

            switch (choise) {
                case 1:
                    System.out.println("\n\rInsert a number");
                    int val = in.nextInt();
                    tree.insert(val);
                    break;
                case 2:
                    tree.minNode();
                    break;
                case 3:
                    tree.maxNode();
                    break;
                case 6:
                    tree.inorder();
                    break;
                default:
                    break;
            }
        } while (choise >= 1 || choise <= 6);
    }
}
