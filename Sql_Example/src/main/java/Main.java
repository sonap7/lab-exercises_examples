import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {
    Connection conn;

    public static void main(String[] args) throws SQLException {
        Main app = new Main();

        app.connectionToMSSql();
        app.normalDbUsage();
    }

    public void connectionToMSSql() throws SQLException {
        // -------------------------------------------
        // URL format is
        // jdbc:derby:<local directory to save data>
        // -------------------------------------------
        //String dbUrl = "jdbc:derby:c:\\sql\\demo;create=true";
        String dbUrl = "jdbc:sqlserver://localhost:1433;instance=SQLEXPRESS;databaseName=company2;";
        String user="sonap";
        String password="notis1991pk";
        conn = DriverManager.getConnection(dbUrl,user,password);
    }

    public void normalDbUsage()  throws SQLException{
        Statement stmt = conn.createStatement();

        // create table
        try {
            //stmt.executeUpdate("DROP table users" );
            //stmt.executeUpdate("Create table users (id int primary key, name varchar(30),address varchar (30))");

            stmt.executeUpdate("Create table employee (id int primary key, name varchar(30),departmentId int foreign key)");
            stmt.executeUpdate("Create table department (id int primary key, name varchar(30)");
            //stmt.executeUpdate("SELECT employee.id, department.name FROM employee INNER JOIN  department ON employee.departmentID = department.id");
        }
        catch(SQLException sqlExc) {
            //System.out.println("Table exists");
        }

        try {
            // insert 2 rows
//            stmt.executeUpdate("insert into users values (1,'tom', 'khfisias123')");
//            stmt.executeUpdate("insert into users values (2,'peter','tatoiou 43')");
            stmt.executeUpdate("insert into employee values (1, 'Giorgos K.', 1)");
            stmt.executeUpdate("insert into employee values (2, 'Hlias G.', 1)");
            stmt.executeUpdate("insert into employee values (3, 'Dimitris P.', 2)");
            stmt.executeUpdate("insert into employee values (4, 'Gerasimos A.', 2)");

            stmt.executeUpdate("insert into employee values (1, 'Accenture')");
            stmt.executeUpdate("insert into employee values (2, 'CodeHub')");
            stmt.executeUpdate("insert into employee values (3, 'Atypon')");
            stmt.executeUpdate("insert into employee values (4, 'Intrasoft')");

        }
        catch (SQLException sqlExc){
//            System.out.println("Ids already exist");
            sqlExc.printStackTrace();
        }

        // query
        //ResultSet rs = stmt.executeQuery("SELECT * FROM users");

        // print out query result
//        while (rs.next()) {
//            System.out.printf("%d\t%s\n", rs.getInt("id"), rs.getString("name"));
//        }
    }
}