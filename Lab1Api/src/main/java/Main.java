import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        Random random = new Random();
        List<Integer> nums =random.ints().limit(10).sorted().boxed().collect(Collectors.toList());
        IntSummaryStatistics stats = nums.stream().mapToInt(i->i).summaryStatistics();
        System.out.println("The minimum number in List : " + stats.getMin());
        System.out.println("The maximun number in List : " + stats.getMax());
        System.out.println("Sum of all numbers : " + stats.getSum());
        System.out.println("Average of all numbers : " + stats.getAverage() +"\n\r");

        nums.stream().filter(i -> i % 2 != 0).collect(Collectors.toList());
        System.out.println("Odd numbers:\n\r");
        nums.stream().filter(i -> i %2 !=0).forEach(System.out::println);

        System.out.println("\n\rEven numbers:\n\r");
        nums.stream().filter(i -> i % 2 == 0).collect(Collectors.toList());
        nums.stream().filter(i -> i %2 ==0).forEach(System.out::println);



    }
}
