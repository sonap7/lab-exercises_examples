package gr.test.hello.service;

import gr.test.hello.model.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("employeeService")
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

    private static List<Employee> employees;

    static{
        employees = populateDummyUsers();
    }

    public List<Employee> findAllEmployees(){
        return employees;
    }

    @Override
    public Employee findById(long id) {
        return employees.get(((int) id-1));
    }

    @Override
    public void deleteEmployeeById(long id) {
        employees.remove(findById((int) id));
    }

    public static List<Employee> populateDummyUsers(){
        List<Employee> employees = new ArrayList();
        employees.add(new Employee(1,"Panos", 28, 1500));
        employees.add(new Employee(2,"Giorgos", 32, 1800));
        employees.add(new Employee(3,"Ntinos", 25, 1000));
        employees.add(new Employee(4,"Kostas", 26, 1200));
        return employees;
    }
}
