package gr.test.hello.service;

import gr.test.hello.model.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> findAllEmployees();
    Employee findById(long id);

    void deleteEmployeeById(long id);
}
