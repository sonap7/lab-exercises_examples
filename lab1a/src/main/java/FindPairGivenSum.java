import java.util.List;

public class FindPairGivenSum {

    /**
     * The method findPairGivenSum takes as parameters
     * @param list which provides an unsorted list of intergers and
     * @param sum which is the target sum to find a pair and print its index
     */

    public void findPairGivenSum(List<Integer> list, int sum) {

        for (int i = 0; i < list.size(); i++)
            for (int j = i + 1; j < list.size(); j++)
                if (list.get(i) + list.get(j) == sum)
                    System.out.println("Summary is 10: Pair found at index "+i+" and "+j+" ("+ list.get(i) +"+"+ list.get(j) +")");
    }
}
